<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@taglib uri="/struts-tags" prefix="s" %>
<!DOCTYPE html>
<html lang="zh-CN">

    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- 上述3个meta标签*必须*放在最前面，任何其他内容都*必须*跟随其后！ -->
        <title>表格</title>
        <meta name="keywords" content="侧边导航菜单(可分组折叠)">
        <meta name="description" content="侧边导航菜单(可分组折叠)" />
        <meta name="HandheldFriendly" content="True" />
        <link rel="shortcut icon" href="img/favicon.ico">
        <!-- Bootstrap3.3.5 CSS -->
        <link href="${pageContext.request.contextPath}/css/bootstrap.css" rel="stylesheet">

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
            <script src="//cdn.bootcss.com/html5shiv/3.7.2/html5shiv.min.js"></script>
            <script src="//cdn.bootcss.com/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
        <style type="text/css">
        ::-webkit-scrollbar {
display: none;
}


            .panel-group{max-height:770px;overflow: auto;}
            .leftMenu{margin:10px;margin-top:5px;}
            .leftMenu .panel-heading{font-size:14px;padding-left:20px;height:36px;line-height:36px;color:white;position:relative;cursor:pointer;}/*转成手形图标*/
            .leftMenu .panel-heading span{position:absolute;right:10px;top:12px;}
            .leftMenu .menu-item-left{padding: 2px; background: transparent; border:1px solid transparent;border-radius: 6px;}
            .leftMenu .menu-item-left:hover{background:#C4E3F3;border:1px solid #1E90FF;}
            
            #style{
	background:url(${pageContext.request.contextPath}/main/CarIO/img/2.gif);
	background-size:auto 356%;
	}
            
        </style>
    </head>

    <body>
        <div class="row">
            <div class="col-md-2">
                <div class="panel-group table-responsive" style="min-height:350px" role="tablist" >
                    <div class="panel panel-primary leftMenu " >
                        <!-- 利用data-target指定要折叠的分组列表 -->
                        <div class="panel-heading" id="collapseListGroupHeading1" data-toggle="collapse" data-target="#collapseListGroup1" role="tab" >
                            <h4 class="panel-title">
                               停车场区域
                                <span class="glyphicon glyphicon-chevron-up right"></span>
                            </h4>
                        </div>
                        <!-- .panel-collapse和.collapse标明折叠元素 .in表示要显示出来 -->
                        <div id="collapseListGroup1" style="" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="collapseListGroupHeading1">
                            <ul class="list-group">
                            <s:iterator value="pots">
                              <li class="list-group-item">
                              <button class=" btn btn-info" style="background:rgb(51,122,183)" target="right" onclick="top.right.location.href='pot_show?entity=park_name&str=<s:property value="pot_name"/>'"> <span class="glyphicon glyphicon-triangle-right"></span><s:property value="pot_name"/>区</a></button>
                                   <%--  <span class="glyphicon glyphicon-triangle-right"></span><a target="right" href='pot_show?entity=park_name&str=<s:property value="pot_name"/>'><s:property value="pot_name"/>区</a> --%>
                              </li>    
                              </s:iterator>     
                            </ul>
                        </div>
                    </div><!--panel end-->
                    <div class="panel panel-primary leftMenu">
                        <div class="panel-heading" id="collapseListGroupHeading2" data-toggle="collapse" data-target="#collapseListGroup2" role="tab" >
                            <h4 class="panel-title">
                                车辆出入管理
                                <span class="glyphicon glyphicon-chevron-down right"></span>
                            </h4>
                        </div>
                        <div id="collapseListGroup2" class="panel-collapse collapse" role="tabpanel" aria-labelledby="collapseListGroupHeading2">
                            <ul class="list-group">
                              <li class="list-group-item">
                                   <button class=" btn btn-info" style="background:rgb(51,122,183)" target="right" onclick="top.right.location.href='main/CarIO/CarIn.jsp'"> <span class="glyphicon glyphicon-triangle-right"></span>车辆入场录入</button>
                      <%--               <span class="glyphicon glyphicon-triangle-right"></span> <a class="menu-item-left" target="right" href="main/CarIO/CarIn.jsp">车辆入场录入</a> --%>
                              </li>
                                <li class="list-group-item">
                                <button class=" btn btn-info" style="background:rgb(51,122,183)" target="right" onclick="top.right.location.href='park_outlist'"> <span class="glyphicon glyphicon-triangle-right"></span>车辆出场信息</button>
                    <%--                 <span class="glyphicon glyphicon-triangle-right"></span> <a class="menu-item-left" target="right" href="park_outlist">车辆出场信息</a> --%>
                              </li>
                            </ul>
                        </div>
                    </div> 
                      <div class="panel panel-primary leftMenu">
                        <div class="panel-heading" id="collapseListGroupHeading3" data-toggle="collapse" data-target="#collapseListGroup3" role="tab" >
                            <h4 class="panel-title">
                                停车记录管理
                                <span class="glyphicon glyphicon-chevron-down right"></span>
                            </h4>
                        </div>
                        <div id="collapseListGroup3"  class="panel-collapse collapse" role="tabpanel" aria-labelledby="collapseListGroupHeading3">
                            <ul class="list-group" >
                              <li class="list-group-item">
                                    <button class=" btn btn-info" style="background:rgb(51,122,183)" target="right" onclick="top.right.location.href='park_show'"> <span class="glyphicon glyphicon-triangle-right"></span>停车记录信息</button>
                              </li>
                            </ul>
                        </div>
                    </div> 
                     
                </div>
            </div>
            <div class="container" style="margin-left:10px">
              <button class="btn btn-info" style="background:rgb(51,122,183)" target="right" onclick="top.right.location.href='${pageContext.request.contextPath}/main/addPot.jsp'">添加停车场</button>
            </div>
        </div>
        <!-- jQuery1.11.3 (necessary for Bo otstrap's JavaScript plugins) -->
        <script src="${pageContext.request.contextPath }/js/jquery-1.11.3.min.js "></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="${pageContext.request.contextPath}/js/bootstrap.min.js "></script>
        <script>
        $(function(){
            $(".panel-heading").click(function(e){
                /*切换折叠指示图标*/
                $(this).find("span").toggleClass("glyphicon-chevron-down");
                $(this).find("span").toggleClass("glyphicon-chevron-up");
            });
        });
        </script>
    </body>

</html>