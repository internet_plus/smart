<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ page import="java.util.*,java.text.*"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<link rel="stylesheet" href="<%=request.getContextPath()%>/css/bootstrap.css" type="text/css"></link>
<script type="text/javascript" src="<%=request.getContextPath()%>/js/jquery-1.8.3.js"></script>
<link rel="stylesheet" href="<%=request.getContextPath()%>/css/main.css" type="text/css"></link></head>
<style type="text/css">
	body{
	padding:"40px"
	}
	.tablelimit{
	min-width:900px;
		width:70%;
		margin:0 auto;
	}
</style>
</head>
<body>
<div class="container" style="padding:40px"> 
<h1> <span class="label label-primary">停车场信息录入</span></h1> 
<hr class="dirver"/>  
</div>  
<hr class="hr"/>
<div class="tablelimit">
	<form action="pot_add" method="post">
	<table class="table" border="1" cellpadding="0" cellspacing="0" align="center" width="600px">
		<tr>
		<td>
			<div class="input-group">
				<span class="input-group-addon">当前城市</span>
			<input class="form-control" type="text" name="pot.pot_city"/>
			</div> 
			</td>
		</tr>
		<tr><td>
		<div class="input-group col-lg-6">
			<span class="input-group-addon">停车场编号</span>
			<input class="form-control" type="text" name="pot.pot_name" onkeyup="this.value=this.value.toUpperCase();" /> 
			</div></td>
		</tr>
		<tr><td>
		<div class="input-group col-lg-6">
		<span class="input-group-addon" id="basic-addon1">停车场类型</span>
		<span class="input-group-addon">
        <input type="radio" name="pot.pot_type"  value="1" aria-label="...">
      </span>
      <input type="text" class="form-control" value="1" readonly="true" aria-label="...">
      <span class="input-group-addon">
        <input type="radio" name="pot.pot_type"  value="2" aria-label="...">
      </span>
      <input type="text" class="form-control" value="2" readonly="true" aria-label="...">
      <span class="input-group-addon">
        <input type="radio" name="pot.pot_type"  value="3" aria-label="...">
      </span>
      <input type="text" class="form-control" value="3" readonly="true" aria-label="...">
			
			</div></td>
		</tr>
		
		<tr>
			<td>
			<div class="input-group col-lg-6">
			<span class="input-group-addon" id="basic-addon1">最大容量</span>
			<span class="input-group-addon">
        <input type="radio" name="pot.pot_max"  value="30" aria-label="...">
      </span>
        <input type="text" class="form-control" value="30" readonly="true" aria-label="...">
        	<span class="input-group-addon">
        <input type="radio" name="pot.pot_max"  value="50" aria-label="...">
      </span>
        <input type="text" class="form-control" value="50" readonly="true" aria-label="...">
        	<span class="input-group-addon">
        <input type="radio" name="pot.pot_max"  value="100" aria-label="...">
      </span>
        <input type="text" class="form-control" value="100" readonly="true" aria-label="...">
        	<span class="input-group-addon">
        <input type="radio" name="pot.pot_max"  value="150" aria-label="...">
      </span>
        <input type="text" class="form-control" value="150" readonly="true" aria-label="...">
			</div>
			</td>
		</tr>
		<tr>
			<td>	
		<button class="btn btn-primary" type="button" onclick="form.submit();">录入</button>
		</tr>
	
	</table>
	</form>
	</div>
</body>
</html>