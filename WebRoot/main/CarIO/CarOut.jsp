<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="java.util.*,java.text.*,com.zovi.spms.entity.Park"%>
<%@taglib uri="/struts-tags" prefix="s" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" href="${pageContext.request.contextPath}/css/bootstrap.css" type="text/css"></link>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery-1.8.3.js"></script>
<title>Insert title here</title>
<style type="text/css" >
body{
	background:url(${pageContext.request.contextPath}/main/CarIO/img/2.gif);
	background-size:auto 206%;
}
	#center{
		margin: 0 auto;
		width:900px;
	}
	
table.t2{
	width:300px;
	height:400px;
	border:1px solid #9db3c5;
	color:#666;
}
table.t2 th {
	height:40px;
	color:#fff;
}
table.t2 td{
	font-size:14px;
	border:1px dotted #cad9ea;
	padding:0 2px 0;
}
table.t2 th{
	width:100%;
	background:rgba(51,122,183,1);
	text-align:center;
	border:1px solid #a7d1fd;
	border-radius:5px;
	padding:0 2px 0;
}
table.t2 tr {
	background-color:#e8f3fd;
}
table input[type='text']{
	height:44px;
	background:rgb(204,204,204);
}
.bg{
background:rgb(204,204,204);
}
.bg input[type="text"]{
background:rgb(204,204,204);
}
</style>

</head>
<body>
			<%
			Park p = (Park) request.getAttribute("park");
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat dateFormat = 
             new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
             String a ="2018-3-14 00:00:00";
             Date date = dateFormat.parse(p.getPark_longitude());
             Date nowdate = dateFormat.parse(dateFormat.format(calendar.getTime()));
             System.out.print(date+"***"+nowdate);
		long total = (nowdate.getTime() - date.getTime())/1000;
		int day = (int)(total / (24*60*60));
		long afterDay = total - day*24*60*60;
		int hour = (int)(afterDay/(60*60));
		long afterHour =  total - day*24*60*60 - hour*60*60;
		int min = (int)(afterHour/60);
		long afterMin = total - day*24*60*60 - hour*60*60 - min*60;
		int differ = (int)(day*24+hour+(afterMin+min*60)/3600+1);
        float money = (float)(differ*p.getPark_standard());
			 %>
  <script type="text/javascript">
   $(function(){
var date1 = new Date('<s:property value="park.park_longitude" />');
var date2 = new Date('<%=dateFormat.format(calendar.getTime())%>');
var s1 = date1.getTime(),s2 = date2.getTime();
var total = (s2 - s1)/1000;
var day = parseInt(total / (24*60*60));
var afterDay = total - day*24*60*60;
var hour = parseInt(afterDay/(60*60));
var afterHour = total - day*24*60*60 - hour*60*60;
var min = parseInt(afterHour/60);
var afterMin = total - day*24*60*60 - hour*60*60 - min*60;
var diff = day*24+hour+(afterMin+min*60)/3600+1;
	$("#fee").val((diff * <s:property value="park.park_standard"/>).toFixed(2)+"元");
	$("#latest").val(hour+day*24+"小时");
      });
</script>
<div id="center">
<br><br><br>
	<form action="park_out_show" method="post">
	
	<table class="t2" border="1" cellpadding="0" cellspacing="0" align="center" >
		<tr>
		<th class="container">
			<h3 style="width:100%"><span class="label label-primary">停车详情</span></h3>
		</th>
		</tr>
		<tr >
			<td><span class="input-group-addon">车主账号：</span><input class="form-control" type="text" name="park.park_user_id" value='<s:property value="park.park_user_id" />' readonly="readonly"/></td>
		</tr>
		<tr>
			<td><span class="input-group-addon">车牌号：</span><input type="text" class="form-control" name="park.park_car_plate" value='<s:property value="park.park_car_plate" />' readonly="readonly"/></td>
		</tr>
		<tr>
			<td>
			<span class="input-group-addon">收费标准：</span><input type="text" class="form-control" name="park.park_standard" value='<s:property value="park.park_standard" />'  readonly="readonly"/>
			</td>
		</tr>
		<tr>
			<td><span class="input-group-addon">备注信息：</span><input type="text" class="form-control" name="park.park_note" value='<s:property value="park.park_note" />' readonly="readonly"/> </td>
		</tr>
		<tr>
			<td><span class="input-group-addon">停车时长：</span><input id="latest" class="form-control" type="text" value=" "  readonly="readonly"/> </td>
		</tr>
		<tr>
			<td><span class="input-group-addon">应付金额：</span><input id="fee" class="form-control" type="text" name="park.park_fee" value="<%=money %>"  readonly="readonly"/> </td>
		</tr>
			<tr>
				<td><span class="input-group-addon">收费员:</span><input type="text" class="form-control" name="park.park_casher" value='<s:property value="park.park_casher" />' readonly="readonly"/> </td>
			</tr>
		<tr>
			
			 <th>	
		<input type="hidden" name="park.park_id" value="<s:property value="park.park_id" />"/>
		<input type="hidden" name="park.park_latitude" value="<%=dateFormat.format(calendar.getTime())%>"/>
		<input type="hidden" name="park.park_longitude" value = '<s:property value="park.park_longitude" />'/>
		<input type="hidden" name="park.park_type" value="<s:property value="park.park_type" />"/>
		<input type="hidden" name="park.park_max" value="50"/>
		<input type="hidden" name="park.park_name" value="null"/>
		<button class="btn btn-primary btn-sm"  type="button" onclick="form.submit();">确认出场</button>
		</th>
		</tr>
	
	</table>
	</form>
	</div>
</body>
</html>