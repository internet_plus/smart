<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="java.util.*,java.text.*"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" href="../../css/bootstrap.css" type="text/css"></link>
<title>Insert title here</title>
<style type="text/css">
	body{
	padding:"40px";
	background:url(${pageContext.request.contextPath}/main/CarIO/img/2.gif);
	background-size:auto 206%;
	}
	.tablelimit{
	min-width:900px;
		width:70%;
		margin:0 auto;
	}
	table{
	background:rgb(249,249,249);}
</style>
</head>
<body>
<div class="container" style="padding:40px"> 
<h1> <span class="label label-primary">车辆信息录入</span></h1> 
<hr class="dirver"/>  
</div>  
<hr class="hr"/>
<div class="tablelimit">
	<form action="park_add" method="post" enctype="multipart/form-data">
	<table class="table table-bordered table-striped" border="1" cellpadding="0" cellspacing="0" align="center" width="600px">
		<tr>
		<td>
		<div class="input-group" style="padding-left:16px">
  <span class="input-group-addon" id="basic-addon1" >车主账号：</span>
  <input type="text" class="form-control" name="park.park_user_id" placeholder="请输入车主账号" aria-describedby="basic-addon1">
		</div>
			</td>
		</tr>
		<tr>
		<td>
		<div class="input-group" style="padding-left:16px">
  <span class="input-group-addon" id="basic-addon1">车牌号：</span>
  <input type="text" class="form-control" name="park.park_car_plate" placeholder="请输入车牌号" aria-describedby="basic-addon1">
		</div>
			</td>
		</tr>
		<tr>
		<td>
		<div class="input-group" style="padding-left:16px">
  <span class="input-group-addon" id="basic-addon1">车牌号照片：</span>
  <input type="file" class="form-control" name="upload" aria-describedby="basic-addon1">
		</div>
			</td>
		</tr>
		<tr>
			<td><div class="input-group" style="padding-left:16px">
  <span class="input-group-addon" id="basic-addon1">当前城市：</span>
  <input type="text" class="form-control" name="park.park_city" placeholder="请输入当前城市" aria-describedby="basic-addon1">
		</div></td>
		</tr>
		<tr>
			<td>
			 <div class="col-sm-6 ">
    <div class="input-group">
      <span class="input-group-addon" id="basic-addon1">停车区域：</span>
      <span class="input-group-addon">
        <input type="radio" name="park.park_name"  value="A" aria-label="...">
      </span>
      <input type="text" class="form-control" value="A" readonly="true" aria-label="...">
       <span class="input-group-addon">
        <input type="radio" name="park.park_name" value="B"  aria-label="...">
      </span>
      <input type="text" class="form-control" value="B" readonly="true" aria-label="...">
       <span class="input-group-addon">
        <input type="radio" name="park.park_name" value="C"  aria-label="...">
      </span>
      <input type="text" class="form-control" value="C" readonly="true" aria-label="...">
    </div><!-- /input-group -->
  </div><!-- /.col-lg-6 -->
			</td>
		</tr>
		<tr>
			<td>
			 <div class="col-sm-6">
    <div class="input-group">
      <span class="input-group-addon" id="basic-addon1">停车类型：</span>
      <span class="input-group-addon">
        <input type="radio" name="park.park_type"  value="1" aria-label="...">
      </span>
      <input type="text" class="form-control" value="1" readonly="true" aria-label="...">
       <span class="input-group-addon">
        <input type="radio" name="park.park_type" value="2"  aria-label="...">
      </span>
      <input type="text" class="form-control" value="2" readonly="true" aria-label="...">
       <span class="input-group-addon">
        <input type="radio" name="park.park_type" value="3"  aria-label="...">
      </span>
      <input type="text" class="form-control" value="3" readonly="true" aria-label="...">
    </div><!-- /input-group -->
  </div><!-- /.col-lg-6 -->
			</td>
		</tr>
		<tr>
			<td>
			 <div class="col-sm-6">
    <div class="input-group">
     <span class="input-group-addon" id="basic-addon1">车主账号：</span>
      <span class="input-group-addon">
        <input type="radio" name="park.park_standard"  value="3" aria-label="...">
      </span>
      <input type="text" class="form-control" value="3元/小时" readonly="true" aria-label="...">
       <span class="input-group-addon">
        <input type="radio" name="park.park_standard" value="5"  aria-label="...">
      </span>
      <input type="text" class="form-control" value="5元/小时" readonly="true" aria-label="...">
       <span class="input-group-addon">
        <input type="radio" name="park.park_standard" value="10"  aria-label="...">
      </span>
      <input type="text" class="form-control" value="10元/小时" readonly="true" aria-label="...">
    </div><!-- /input-group -->
  </div><!-- /.col-lg-6 -->
			</td>
		</tr>
		<tr>
			<td> <div class="input-group" style="padding-left:16px">
  <span class="input-group-addon" id="basic-addon1">备注信息：</span>
  <input type="text" class="form-control" name="park.park_note" placeholder="备注信息" aria-describedby="basic-addon1">
		</div>
		</tr>
	
		<tr>
			<td colspan="2">	
			<%
		int year, month, day, week, hh;
        Calendar calendar = Calendar.getInstance();
        year = calendar.get(Calendar.YEAR);
        month = calendar.get(Calendar.MONTH);
        day = calendar.get(Calendar.DAY_OF_MONTH);
        week = calendar.get(Calendar.DAY_OF_WEEK);
        hh = calendar.get(Calendar.HOUR);
        SimpleDateFormat dateFormat = 
             new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        System.out.println("date=" + dateFormat.format(calendar.getTime()));
        System.out.println("year=" + year);
        System.out.println("month=" + month);
        System.out.println("day=" + day);
        System.out.println("week=" + week);
        System.out.println("hh=" + hh);
			 %>
		<input type="hidden" name="a" value=""/>
		<input type="hidden" name="park.park_longitude" value="<%=dateFormat.format(calendar.getTime())%>"/>
		<input type="hidden" name="park.park_latitude" value="0"/>
		<input type="hidden" name="park.park_casher" value="z"/>
		<input type="hidden" name="park.park_max" value="50"/>
		<button type="button" class="btn btn-primary" onclick="form.submit();">提交信息</button></td>
		</tr>
	
	</table>
	</form>
	</div>
</body>
</html>