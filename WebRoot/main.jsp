<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ page import = "com.zovi.spms.entity.Park,java.util.*" %>
<%@taglib uri="/struts-tags" prefix="s" %>
<%@page import= "java.util.*,java.text.*" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<link rel="stylesheet" href="${pageContext.request.contextPath}/css/bootstrap.css" type="text/css"></link>
<link rel="stylesheet" href="${pageContext.request.contextPath}/css/main.css" type="text/css"></link></head>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery-1.8.3.js"></script>
<style type="text/css">
	body{
		min-width: 1210px;
		padding:12px;
	background:url(${pageContext.request.contextPath}/main/CarIO/img/2.gif);
	background-size:auto 356%;
	}
		.tablelimit{
	min-width:900px;
		width:70%;
		margin:0 auto;
	}
</style>
	<%
		int year, month, day, week, hh;
        Calendar calendar = Calendar.getInstance();
        year = calendar.get(Calendar.YEAR);
        month = calendar.get(Calendar.MONTH);
        day = calendar.get(Calendar.DAY_OF_MONTH);
        week = calendar.get(Calendar.DAY_OF_WEEK);
        hh = calendar.get(Calendar.HOUR);
        SimpleDateFormat dateFormat = 
             new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			 %>
<body>
<div class="container" style="padding:40px"> 
<h1> <span class="label label-primary">停车记录信息</span></h1> 
<hr class="dirver"/>  
</div>  
 <script type="text/javascript">
   var int=self.setInterval("clock()",100);
function clock()
  {
  var t=new Date();
  t = t.replace(/-/g,"/");
  $("#aa").text(t);
  }
</script>
<div id="aa"></div>
<% String k = (String)request.getParameter("key"); %>
<div class="col-lg-6">
<form action="park_show" method="post">
	 <div class="input-group"  style="margin-bottom:20px">
      <input type="text" class="form-control " placeholder="请输入车牌号..." name="key" value="<%if(k!=null)out.print(k);%>" />
      <input type="hidden" name="domain" value="park_car_plate" />
      <span class="input-group-btn">
        <button class="btn btn-default" style="height:34px" type="button" onclick="form.submit();"><span class="glyphicon glyphicon-search"></span></button>
      </span>
    </div>
</form>
</div>
<table class="table">
    <thead>
    <tr>
        <th>ID</th>
        <th>停车场区域</th>
        <th>车辆车牌</th>
        <th>车主账号</th>
        <th>入场时间</th>
        <th>出场时间</th>
        <th>当前应付金额</th>
        <th>停车类型</th>
        <th>停车类型/单价</th>
        <th>停车备注</th>
        <th>收费员</th>
    </tr>
    </thead>
    <tbody class="sp_tb">
    
  <s:iterator value="list" status="pk" var="p">
    	<tr class='a'>
        <td><s:property value="#p.park_id" /></td>
		<td id="b<s:property value="#pk.getCount()"/>"><s:if test="#p.park_name!='null'"><s:property value="#p.park_name" /></s:if><s:else>已出场</s:else></td>
        <td><s:property value="#p.park_car_plate" /></td>
        <td><s:property value="#p.park_user_id" /></td>
        <td><s:property value="#p.park_longitude" /></td>
        <td>
        <s:if test="#p.park_name!='null'">
        <button style="button" class="btn btn-default" onclick="location.href='park_out?park_id=<s:property value="#p.park_id" />'">设置出场</button>
        </s:if>
        <s:else>
     	<s:property value="#p.park_latitude" />
        </s:else>
        </td>
        <td id='a<s:property value="#pk.getCount()"/>'>
        --
        </td>
        <td><s:property value="#p.park_type" /></td>
        <td><s:property value="#p.park_standard" /></td>
        <td><s:property value="#p.park_note" /></td>
        <td><s:property value="#p.park_casher" /></td>        
    </tr>
   <script type="text/javascript">
   $(function(){
var date1 = new Date('<s:property value="park_longitude" />')
var date2 = new Date('<%=dateFormat.format(calendar.getTime())%>')
var s1 = date1.getTime(),s2 = date2.getTime();
var total = (s2 - s1)/1000;
var day = parseInt(total / (24*60*60));
var afterDay = total - day*24*60*60;
var hour = parseInt(afterDay/(60*60));
var afterHour = total - day*24*60*60 - hour*60*60;
var min = parseInt(afterHour/60);
var afterMin = total - day*24*60*60 - hour*60*60 - min*60;
var diff = day*24+hour+(afterMin+min*60)/3600+1;
if($("#b<s:property value='#pk.getCount()'/>").text()==="已出场"){
	$("#a<s:property value='#pk.getCount()'/>").text("已付款");
	}else{
	$("#a<s:property value='#pk.getCount()'/>").text((diff*<s:property value="park_standard" />).toFixed(2));
   }
      })
</script>
</s:iterator>
    </tbody>
</table> 
<s:if test="totalCount==0">
<s:if test='search == "y"'>
<div class="container" style="padding:40px"> 
<h1> <span class="label label-info">未查询到结果~ </span></h1> 
<hr class="dirver"/>  
</div>  
</s:if>
<s:else>
<div class="container" style="padding:40px"> 
<h1> <span class="label label-info">目前还没有车停在这里哦~ </span></h1> 
<hr class="dirver"/>  
</div>  
</s:else>
</s:if>
<s:if test="totalCount!=0">
<table border="0" cellspacing="0" cellpadding="0" align="center">
	<tr class="page">
		<td align="right">
			<span>第<s:property value="currPage"/>/<s:property value="totalPage"/>页</span>&nbsp;&nbsp;
			<span>总记录数:<s:property value="totalCount"/>&nbsp;&nbsp;每页显示:<s:property value="pageSize"/></span>&nbsp;&nbsp;
				<s:if test="currPage != 1">
				<a href="${pageContext.request.contextPath}/park_show?currPage=1 "><span class="glyphicon glyphicon-triangle-left"></span></a>&nbsp;&nbsp;
				<a href="${pageContext.request.contextPath}/park_show?currPage=<s:property value="currPage-1"/> "><span class="glyphicon glyphicon-menu-left"></span></a>
				</s:if>
				<s:if test="currPage != totalPage">
				<a href="${pageContext.request.contextPath}/park_show?currPage=<s:property value="currPage+1"/> "><span class="glyphicon glyphicon-menu-right"></span></a>&nbsp;&nbsp;
				<a href="${pageContext.request.contextPath}/park_show?currPage=<s:property value="totalPage"/>"><span class="glyphicon glyphicon-triangle-right"></span></a>
				</s:if>
		</td>
	</tr>
</table>
</s:if>

<script>
	
	  
$(document).ready(function(){
 $("tr:not(.page):even").addClass("even");
 $("tr:not(.page):odd").addClass("odd");
  $("tr:first").css("background","white");
 //点击变色 

 $("tr:not(.page)").toggle(
 function(){
 $(this).addClass("selected");
 },function (){
 $(this).removeClass("selected");
 }
 );
 
 //滑动变色
 $("tr:not(.page)").mouseover(function (){
 $(this).addClass("se"); 
 }).mouseout(function (){
 $(this).removeClass("se");
 });
});

</script>
</body>
</html>