/*
Navicat MySQL Data Transfer

Source Server         : Aoshen
Source Server Version : 50096
Source Host           : localhost:3306
Source Database       : smart_park

Target Server Type    : MYSQL
Target Server Version : 50096
File Encoding         : 65001

Date: 2018-03-22 22:16:30
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for t_park
-- ----------------------------
DROP TABLE IF EXISTS `t_park`;
CREATE TABLE `t_park` (
  `park_id` int(11) NOT NULL auto_increment,
  `park_name` varchar(5) default NULL,
  `park_type` int(11) NOT NULL,
  `park_standard` double(5,1) default NULL,
  `park_note` varchar(100) NOT NULL,
  `park_city` varchar(15) default NULL,
  `park_max` int(11) NOT NULL,
  `park_longitude` varchar(50) default NULL,
  `park_latitude` varchar(50) default NULL,
  `park_casher` varchar(12) NOT NULL,
  `park_car_plate` varchar(10) NOT NULL,
  `park_username` varchar(22) default NULL,
  `park_user_id` varchar(20) default NULL,
  `park_fee` varchar(20) default NULL,
  PRIMARY KEY  (`park_id`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_park
-- ----------------------------
INSERT INTO `t_park` VALUES ('1', 'null', '1', '5.0', '红红火火恍恍惚惚', '郑州', '50', '2018-03-10 20:27:11', '2018-03-14 03:33:30', 'z', '豫ASB251', null, '201803210006', '400.53元');
INSERT INTO `t_park` VALUES ('2', 'null', '1', '5.0', '只是一辆普通的车', '郑州', '50', '2018-03-11 01:45:15', '2018-03-14 03:47:28', 'z', '豫BBS222', null, '201803210005', '375.18元');
INSERT INTO `t_park` VALUES ('3', 'null', '2', '2.0', '红红火火恍恍惚惚', '郑州', '50', '2018-03-11 01:45:29', '2018-03-14 14:09:03', 'z', '京ABC666', null, 'SVIP02310004', '170.79元');
INSERT INTO `t_park` VALUES ('4', 'null', '2', '6.0', '好车', null, '50', '2018-03-11 01:45:46', '2018-03-22 20:07:13', 'z', '豫NSB222', null, 'SVIP02310003', '1700.14元');
INSERT INTO `t_park` VALUES ('5', 'null', '1', '10.0', '只是一辆普通的车', '郑州', '50', '2018-03-11 21:26:47', '2018-03-14 04:19:40', 'z', '京ABS123', null, 'SVIP02310005', '558.81元');
INSERT INTO `t_park` VALUES ('6', 'null', '1', '3.0', '都没看附件', null, '50', '2018-03-12 15:39:10', '2018-03-22 20:08:14', 'z', '豫NBS272', null, '201803210009', '736.45元');
INSERT INTO `t_park` VALUES ('7', 'null', '3', '10.0', '只是一辆普通的车', '郑州', '50', '2018-03-13 01:29:31', '2018-03-14 03:51:28', 'z', '豫ABS222', null, '201803210008', '273.66元');
INSERT INTO `t_park` VALUES ('8', 'B', '2', '10.0', '红红火火恍恍惚惚', '郑州', '50', '2018-03-13 01:30:17', '0', 'z', '豫ABS652', null, 'SVIP02310002', null);
INSERT INTO `t_park` VALUES ('9', 'B', '2', '3.0', '好车', '郑州', '50', '2018-03-13 01:30:33', '0', 'z', '豫BBS225', null, '201803210014', null);
INSERT INTO `t_park` VALUES ('10', 'B', '2', '5.0', '这是个老实人，大家快骗他。', '郑州', '50', '2018-03-13 01:39:42', '0', 'z', '豫NBS222', null, '201803210015', null);
INSERT INTO `t_park` VALUES ('11', 'B', '2', '10.0', '好车', '郑州', '50', '2018-03-14 03:51:08', '0', 'z', '豫NS222', null, '201803210016', null);
INSERT INTO `t_park` VALUES ('12', 'B', '2', '5.0', 'asd', '郑州', '50', '2018-03-14 18:06:09', '0', 'ez', '豫ABS222', null, 'SVIP02310001', null);
INSERT INTO `t_park` VALUES ('13', 'A', '1', '3.0', '红红火火恍恍惚惚', '郑州', '50', '2018-03-20 19:44:29', '0', 'z', '豫ABS555', null, '201803210017', null);
INSERT INTO `t_park` VALUES ('14', 'B', '1', '3.0', '好车', '郑州', '50', '2018-03-21 15:34:16', '0', 'z', '豫BBS222', null, '201803210018', null);
INSERT INTO `t_park` VALUES ('15', 'B', '2', '5.0', '只是一辆普通的车', '郑州', '50', '2018-03-21 16:14:15', '0', 'z', '豫ASB250', null, '201803210013', null);
INSERT INTO `t_park` VALUES ('16', 'A', '1', '3.0', '只是一辆普通的车', '郑州', '50', '2018-03-21 17:50:57', '0', 'z', '豫NAS230', null, '201803210010', null);
INSERT INTO `t_park` VALUES ('17', 'A', '1', '5.0', '这是个老实人，大家快骗他。', '郑州', '50', '2018-03-21 17:53:02', '0', 'z', '豫AAS231', null, '201803210012', null);
INSERT INTO `t_park` VALUES ('18', 'C', '3', '10.0', '只是一辆普通的车', '郑州', '50', '2018-03-21 17:53:42', '0', 'z', '豫NAS530', null, '201803210003', null);
INSERT INTO `t_park` VALUES ('19', 'B', '2', '5.0', '好车', '郑州', '50', '2018-03-21 17:54:08', '0', 'z', '豫NBS355', null, '201803210004', null);
INSERT INTO `t_park` VALUES ('20', 'C', '3', '10.0', '红红火火恍恍惚惚', '郑州', '50', '2018-03-21 17:55:14', '0', 'z', '豫NAB730', null, '201803210007', null);
INSERT INTO `t_park` VALUES ('21', 'C', '3', '10.0', '这是个土豪', '郑川', '50', '2018-03-21 17:55:56', '0', 'z', '豫N66666', null, '201803210101', null);
INSERT INTO `t_park` VALUES ('22', 'A', '1', '3.0', '好车', '郑州', '50', '2018-03-22 20:20:44', '0', 'z', '豫NSS234', null, 'SVIP201803220', null);
INSERT INTO `t_park` VALUES ('23', 'A', '1', '3.0', 'VIP用户', '郑州', '50', '2018-03-22 21:21:22', '0', 'z', '豫NSS747', null, 'SVIP02310011', null);
INSERT INTO `t_park` VALUES ('24', 'A', '3', '10.0', 'VIP用户', '郑州', '50', '2018-03-22 21:30:40', '0', 'z', '豫NSS959', null, 'SVIP02310022', null);
INSERT INTO `t_park` VALUES ('25', 'C', '3', '10.0', '只是一辆普通的车', '郑州', '50', '2018-03-22 21:51:11', '0', 'z', '豫AAS330', null, 'SVIP02310004', null);
INSERT INTO `t_park` VALUES ('26', 'C', '3', '10.0', '只是一辆普通的车', '郑州', '50', '2018-03-22 21:54:35', '0', 'z', '豫NSS277', null, '201803210008', null);
INSERT INTO `t_park` VALUES ('27', 'C', '3', '3.0', '只是一辆普通的车', '郑州', '50', '2018-03-22 21:54:35', '0', 'z', '豫NSS255', null, '201803210118', null);
INSERT INTO `t_park` VALUES ('28', 'A', '1', '3.0', '只是一辆普通的车', '郑州', '50', '2018-03-22 21:59:54', '0', 'z', '豫NBB245', null, '201803210052', null);
INSERT INTO `t_park` VALUES ('29', 'B', '2', '5.0', '只是一辆普通的车', '郑州', '50', '2018-03-22 22:05:12', '0', 'z', '豫AAS230', null, '201803210062', null);
INSERT INTO `t_park` VALUES ('30', 'C', '2', '10.0', '好车', '郑州', '50', '2018-03-22 22:10:56', '0', 'z', '豫BSS233', null, '201803210063', null);

-- ----------------------------
-- Table structure for t_pot
-- ----------------------------
DROP TABLE IF EXISTS `t_pot`;
CREATE TABLE `t_pot` (
  `pot_id` int(11) NOT NULL auto_increment,
  `pot_type` int(11) NOT NULL,
  `pot_max` int(11) NOT NULL,
  `pot_city` varchar(20) default NULL,
  `pot_name` varchar(5) default NULL,
  PRIMARY KEY  (`pot_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_pot
-- ----------------------------
INSERT INTO `t_pot` VALUES ('1', '1', '50', 'zz', 'A');
INSERT INTO `t_pot` VALUES ('2', '2', '100', 'zz', 'B');
INSERT INTO `t_pot` VALUES ('6', '3', '50', '北京', 'C');
INSERT INTO `t_pot` VALUES ('7', '1', '30', '郑州', 'D');
