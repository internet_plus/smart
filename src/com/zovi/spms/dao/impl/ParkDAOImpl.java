
package com.zovi.spms.dao.impl;
import java.util.List;

import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;
import org.springframework.orm.hibernate3.HibernateTemplate;

import com.zovi.spms.dao.ParkDAO;
import com.zovi.spms.entity.Park;
@SuppressWarnings("all")
public class ParkDAOImpl implements ParkDAO {
	private HibernateTemplate hibernateTemplate;
	private Park park;
	
	public Park getPark() {
		return park;
	}

	public void setPark(Park park) {
		this.park = park;
	}

	public HibernateTemplate getHibernateTemplate() {
		return hibernateTemplate;
	}

	public void setHibernateTemplate(HibernateTemplate hibernateTemplate) {
		this.hibernateTemplate = hibernateTemplate;
	}

	public void add(Park park) {
		hibernateTemplate.save(park);
	}

	public void update(Park park) {
		hibernateTemplate.update(park);
	}
	public List<Park> showAll() {
		String hql = "from Park";
		return hibernateTemplate.find(hql);
	}

	public int findCount() {
		String hql = "select count(*) from Park";
		List<Long> list = hibernateTemplate.find(hql);
		if(list.size() > 0)
			return list.get(0).intValue();
		return 0;
	}
	@Override
	public Park findById(Integer id) {
		return hibernateTemplate.get(Park.class, id);
	}

	@Override
	public int findSearchCount(String entity,String str) {
		String hql = "select count(*) from Park where "+entity+" like ? ";
		List<Long> list = hibernateTemplate.find(hql,"%"+str+"%");
		if(list.size() > 0)
			return list.get(0).intValue();
		return 0;
	}

	public List<Park> findByPage(int begin, int pageSize) {
		DetachedCriteria criteria = DetachedCriteria.forClass(Park.class);
		List<Park> list = hibernateTemplate.findByCriteria(criteria,begin,pageSize);
		return list;
	}

	@Override
	public List<Park> searchByString(int begin, int pageSize,String entity,String str) {
		DetachedCriteria criteria = DetachedCriteria.forClass(Park.class);
		criteria.add(Restrictions.like(entity,"%"+str+"%",MatchMode.ANYWHERE));
		List<Park> lists = hibernateTemplate.findByCriteria(criteria,begin,pageSize);
		return lists;
	}







}
