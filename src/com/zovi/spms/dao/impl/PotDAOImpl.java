package com.zovi.spms.dao.impl;

import java.util.List;

import org.hibernate.criterion.DetachedCriteria;
import org.springframework.orm.hibernate3.HibernateTemplate;

import com.zovi.spms.dao.PotDAO;
import com.zovi.spms.entity.Park;
import com.zovi.spms.entity.Pot;
@SuppressWarnings("all")
public class PotDAOImpl implements PotDAO {
	private HibernateTemplate hibernateTemplate;
	private Pot pot;
	public HibernateTemplate getHibernateTemplate() {
		return hibernateTemplate;
	}

	public void setHibernateTemplate(HibernateTemplate hibernateTemplate) {
		this.hibernateTemplate = hibernateTemplate;
	}

	public Pot getPot() {
		return pot;
	}

	public void setPot(Pot pot) {
		this.pot = pot;
	}

	@Override
	public void add(Pot pot) {
		hibernateTemplate.save(pot);
	}

	@Override
	public void update(Pot pot) {
		hibernateTemplate.update(pot);
	}

	@Override
	public Pot findByPotName(String potname) {
		String hql = "from Pot where pot_name = ?";
		List l = hibernateTemplate.find(hql,potname);
		if(l.size()>0){
		pot.setPot_city((String)((Pot) l.get(0)).getPot_city());
		pot.setPot_max((Integer)((Pot) l.get(0)).getPot_max());
		pot.setPot_name((String)((Pot) l.get(0)).getPot_name());
		pot.setPot_type((Integer)((Pot) l.get(0)).getPot_type());
		}
		return pot;
	}
	public List<Pot> showAll(){
		String hql="from Pot";
		return hibernateTemplate.find(hql);
	}

	@Override
	public int findCount() {
		String hql = "select count(*) from Pot";
		List<Long> list = hibernateTemplate.find(hql);
		if(list.size() > 0)
			return list.get(0).intValue();
		return 0;
	}

	@Override
	public List<Pot> findByPage(int begin, int pageSize) {
		DetachedCriteria criteria = DetachedCriteria.forClass(Park.class);
		List<Pot> list = hibernateTemplate.findByCriteria(criteria,begin,pageSize);
		return list;
	}

}
