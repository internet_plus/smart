package com.zovi.spms.dao;

import java.util.List;

import com.zovi.spms.entity.Pot;

public interface PotDAO {
	public void add(Pot pot);
	public void update(Pot pot);
	public Pot findByPotName(String potname);
	public List<Pot> showAll();
	public int findCount();
	public List<Pot> findByPage(int begin,int pageSize);
}
