package com.zovi.spms.dao;

import java.util.List;

import com.zovi.spms.entity.Park;

public interface ParkDAO {
	public void add(Park park);
	public void update(Park park);
	public List<Park> showAll();
	public Park findById(Integer id);
	public int findCount();
	public int findSearchCount(String entity,String str);
	public List<Park> findByPage(int begin,int pageSize);
	public List<Park> searchByString(int begin,int pageSize,String entity,String str);
}
