package com.zovi.spms.entity;

import javax.persistence.*;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name="t_park")
public class Park {
	@Id
	@GenericGenerator(name="uid",strategy="increment")
	@GeneratedValue(generator="uid")
	private Integer park_id;
	private String park_name;
	private Integer park_type;
	private String park_note;
	private String park_city;
	private Integer park_max;
	private String park_longitude;
	private String park_latitude;
	private String park_casher;
	private String park_car_plate;
	private Double park_standard;
	private String park_user_id;
	private String park_fee;
	
	public String getPark_fee() {
		return park_fee;
	}
	public void setPark_fee(String park_fee) {
		this.park_fee = park_fee;
	}
	public Integer getPark_id() {
		return park_id;
	}
	public void setPark_id(Integer park_id) {
		this.park_id = park_id;
	}
	public Double getPark_standard() {
		return park_standard;
	}
	public void setPark_standard(Double park_standard) {
		this.park_standard = park_standard;
	}
	public String getPark_name() {
		return park_name;
	}
	public void setPark_name(String park_name) {
		this.park_name = park_name;
	}
	public Integer getPark_type() {
		return park_type;
	}
	public void setPark_type(Integer park_type) {
		this.park_type = park_type;
	}
	public String getPark_note() {
		return park_note;
	}
	public void setPark_note(String park_note) {
		this.park_note = park_note;
	}
	public String getPark_city() {
		return park_city;
	}
	
	public void setPark_city(String park_city) {
		this.park_city = park_city;
	}
	public Integer getPark_max() {
		return park_max;
	}
	public void setPark_max(Integer park_max) {
		this.park_max = park_max;
	}

	public String getPark_casher() {
		return park_casher;
	}
	public void setPark_casher(String park_casher) {
		this.park_casher = park_casher;
	}
	public String getPark_car_plate() {
		return park_car_plate;
	}
	public void setPark_car_plate(String park_car_plate) {
		this.park_car_plate = park_car_plate;
	}
	public String getPark_longitude() {
		return park_longitude;
	}
	public void setPark_longitude(String park_longitude) {
		this.park_longitude = park_longitude;
	}
	public String getPark_latitude() {
		return park_latitude;
	}
	public void setPark_latitude(String park_latitude) {
		this.park_latitude = park_latitude;
	}
	public String getPark_user_id() {
		return park_user_id;
	}
	public void setPark_user_id(String park_user_id) {
		this.park_user_id = park_user_id;
	}
	
	
}
