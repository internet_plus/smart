package com.zovi.spms.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name="t_pot")
public class Pot {
	@Id
	@GenericGenerator(name="uid",strategy="increment")
	@GeneratedValue(generator="uid")
	private Integer pot_id;
	private Integer pot_type;
	private Integer pot_max;
	private String pot_city;
	private String pot_name;
	
	public String getPot_name() {
		return pot_name;
	}
	public void setPot_name(String pot_name) {
		this.pot_name = pot_name;
	}
	public Integer getPot_id() {
		return pot_id;
	}
	public void setPot_id(Integer pot_id) {
		this.pot_id = pot_id;
	}
	public Integer getPot_type() {
		return pot_type;
	}
	public void setPot_type(Integer pot_type) {
		this.pot_type = pot_type;
	}
	public Integer getPot_max() {
		return pot_max;
	}
	public void setPot_max(Integer pot_max) {
		this.pot_max = pot_max;
	}
	public String getPot_city() {
		return pot_city;
	}
	public void setPot_city(String pot_city) {
		this.pot_city = pot_city;
	}
	
}
