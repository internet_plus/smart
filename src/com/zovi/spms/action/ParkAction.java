package com.zovi.spms.action;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Date;
import java.util.List;

import org.apache.struts2.ServletActionContext;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;
import com.zovi.spms.entity.Page;
import com.zovi.spms.entity.PageBean;
import com.zovi.spms.entity.Park;
import com.zovi.spms.service.ParkServiceImpl;
@SuppressWarnings("all")
public class ParkAction extends ActionSupport implements ModelDriven<Park>{
	private Park park = new Park();
	private List<Park> parks;
	private ParkServiceImpl parkService;
	private Page page;
	private String str;
	private String domain;
	private String key;
	private String search;
	private File upload;
	private String uploadFileName;
	private String uploadContentType;
	
	
	
	
	public String getUploadContentType() {
		return uploadContentType;
	}

	public void setUploadContentType(String uploadContentType) {
		this.uploadContentType = uploadContentType;
	}

	public String getUploadFileName() {
		return uploadFileName;
	}

	public void setUploadFileName(String uploadFileName) {
		this.uploadFileName = uploadFileName;
	}

	public File getUpload() {
		return upload;
	}

	public void setUpload(File upload) {
		this.upload = upload;
	}

	public String getSearch() {
		return search;
	}

	public void setSearch(String search) {
		this.search = search;
	}

	public String getDomain() {
		return domain;
	}

	public void setDomain(String domain) {
		this.domain = domain;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getStr() {
		return str;
	}

	public void setStr(String str) {
		this.str = str;
	}

	@Override
	public Park getModel() {
		// TODO Auto-generated method stub
		return park;
	}
	private Integer currPage = 1;
	
	public Integer getCurrPage() {
		return currPage;
	}

	public void setCurrPage(Integer currPage) {
		this.currPage = currPage;
	}
	public Page getPage() {
		return page;
	}

	public void setPage(Page page) {
		this.page = page;
	}

	public Park getPark() {
		return park;
	}

	public void setPark(Park park) {
		this.park = park;
	}

	public ParkServiceImpl getParkService() {
		return parkService;
	}

	public void setParkService(ParkServiceImpl parkService) {
		this.parkService = parkService;
	}
	public List<Park> getParks() {
		return parks;
	}
	public void setParks(List<Park> parks) {
		this.parks = parks;
	}
	public String outlist(){
		PageBean<Park> pageBean = parkService.searchByPage(currPage, "park_name", "NULL");
		ActionContext.getContext().getValueStack().push(pageBean);
		return "outlistok";
	}
	public String out(){
		park = parkService.findById(park.getPark_id());
		return "outok";
	}
	public String out_show(){
		parkService.update(park);
		return "showok";
	}
	public String show(){
		if(domain!=null&key!=null){
			 search = "y";
			PageBean<Park> pageBean = parkService.searchByPage(currPage, domain, key);
			 ActionContext.getContext().getValueStack().push(pageBean);
			 return "success";
		}
		 PageBean<Park> pageBean = parkService.findByPage(currPage);
	     //将pageBean存入到值栈中。
		 ActionContext.getContext().getValueStack().push(pageBean);
		return "success";
	}
	public String add(){
		parkService.add(park);
		InputStream input = null;
		OutputStream output  = null;
		if(!this.uploadContentType.equals("image/jpeg")){
/*				resp.getWriter().write("<script>alert('只能添加大小小于5mb的image/jpeg格式图片');location.href='park_show'</script>");*/
			return "addok";
		}else{
		try {
			Date date = new Date();
			uploadFileName = date.getTime()+uploadFileName.substring(uploadFileName.lastIndexOf('.'));
			 System.out.println(ActionContext.getContext());
			input = new FileInputStream(upload);
			String path = ServletActionContext.getServletContext().getRealPath("/upload");
			output = new FileOutputStream(path+"\\"+this.uploadFileName);
			byte[] con = new byte[512];
			int len = 0;
			while(true){
				if((len=input.read(con))==-1){
					break;
				}
				//写出
				output.write(con,0,len);
			}
			output.close();
			input.close();
		}catch (Exception e) {
		} 
		}
		return "addok";
	}
	
}
