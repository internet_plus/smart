package com.zovi.spms.action;

import java.util.List;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;
import com.zovi.spms.entity.Page;
import com.zovi.spms.entity.PageBean;
import com.zovi.spms.entity.Park;
import com.zovi.spms.entity.Pot;
import com.zovi.spms.service.ParkServiceImpl;
import com.zovi.spms.service.PotServiceImpl;

public class PotAction extends ActionSupport implements ModelDriven<Park>{
	private Pot pot = new Pot();
	private Park park = new Park();
	private ParkServiceImpl parkService;
	public void setCurrPage(Integer currPage) {
		this.currPage = currPage;
	}
	private List<Pot> pots;
	private String str;
	private String entity;
	private PotServiceImpl potService;
	private Page page;
	private String domain;
	private String key;
	
	
	public String getDomain() {
		return domain;
	}
	public void setDomain(String domain) {
		this.domain = domain;
	}
	public String getKey() {
		return key;
	}
	public void setKey(String key) {
		this.key = key;
	}
	@Override
	public Park getModel() {
		return park;
	}
	private Integer currPage = 1;
	
	public Integer getCurrPage() {
		return currPage;
	}
	public void setCurrPages(Integer currPage) {
		this.currPage = currPage;
	}
	public Pot getPot() {
		return pot;
	}
	public void setPot(Pot pot) {
		this.pot = pot;
	}
	public List<Pot> getPots() {
		return pots;
	}
	public void setPots(List<Pot> pots) {
		this.pots = pots;
	}
	public PotServiceImpl getPotService() {
		return potService;
	}
	public void setPotService(PotServiceImpl potService) {
		this.potService = potService;
	}
	public Page getPage() {
		return page;
	}
	public void setPage(Page page) {
		this.page = page;
	}
	public String getEntity() {
		return entity;
	}
	public void setEntity(String entity) {
		this.entity = entity;
	}
	public String getStr() {
		return str;
	}
	public void setStr(String str) {
		this.str = str;
	}
	public Park getPark() {
		return park;
	}
	public void setPark(Park park) {
		this.park = park;
	}
	public ParkServiceImpl getParkService() {
		return parkService;
	}
	public void setParkService(ParkServiceImpl parkService) {
		this.parkService = parkService;
	}
	public String show_list(){
		pots = potService.showAll();
		return "pot_list";
	}
	public String show(){
		 PageBean<Park> pageBean = null;
		System.out.println(entity+" "+str);
		if(domain!=null&&key!=null){
			 pageBean = parkService.searchByPage(currPage, domain, key);
			 ActionContext.getContext().getValueStack().push(pageBean);
			 pot = potService.findByPotName(str);
			 return "showok";
			 }
		pageBean = parkService.searchByPage(currPage, entity, str);
		 
	     //将pageBean存入到值栈中。
		 pot = potService.findByPotName(str);
		 ActionContext.getContext().getValueStack().push(pageBean);
		 System.out.println(pageBean);
		return "showok";
	}
	public String add(){
		System.out.println(pot);
		potService.add(pot);
		return "addok";
	}

}
