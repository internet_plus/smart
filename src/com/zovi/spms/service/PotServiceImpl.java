package com.zovi.spms.service;

import java.util.List;

import com.zovi.spms.dao.PotDAO;
import com.zovi.spms.entity.PageBean;
import com.zovi.spms.entity.Pot;

public class PotServiceImpl implements PotService{
	private PotDAO potDAO;
	
	public PotDAO getPotDAO() {
		return potDAO;
	}

	public void setPotDAO(PotDAO potDAO) {
		this.potDAO = potDAO;
	}

	@Override
	public void add(Pot pot) {
	potDAO.add(pot);
	}

	@Override
	public void update(Pot pot) {
		potDAO.update(pot);
		
	}

	@Override
	public Pot findByPotName(String potname) {
		return potDAO.findByPotName(potname);
	}

	@Override
	public List<Pot> showAll(){
		List list = potDAO.showAll();
		return list;
	}
	@Override
	public PageBean<Pot> findByPage(Integer currPage) {
		PageBean<Pot> pageBean = new PageBean<Pot>();
		//封装当前页数
		pageBean.setCurrPage(currPage);
		//封装每页显示记录数
		int pageSize = 10;
		pageBean.setPageSize(pageSize);
		//封装总记录数
		int totalCount = potDAO.findCount();
		pageBean.setTotalCount(totalCount);
		//封装总页数
		double tc = totalCount;
		Double num = Math.ceil(tc / pageSize);
		pageBean.setTotalPage(num.intValue());
		//封装每页显示的数据
		int begin = (currPage - 1) * pageSize;
		List<Pot> list = potDAO.findByPage(begin,pageSize);
		pageBean.setList(list);
		return pageBean;
	}

}
