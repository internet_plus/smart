package com.zovi.spms.service;

import java.util.List;

import com.zovi.spms.entity.PageBean;
import com.zovi.spms.entity.Park;

public interface ParkService {
	public void add(Park park);
	public void update(Park park);
	public Park findById(Integer id);
	public List<Park> showAll();
	public PageBean<Park> findByPage(Integer currPage);
	public PageBean<Park> searchByPage(Integer currPage,String entity,String str);
}
