package com.zovi.spms.service;

import java.util.List;

import com.zovi.spms.entity.PageBean;
import com.zovi.spms.entity.Pot;

public interface PotService {
	public void add(Pot pot);
	public void update(Pot park);
	public Pot findByPotName(String potname);
	public List<Pot> showAll();
	public PageBean<Pot> findByPage(Integer currPage);
}
