package com.zovi.spms.service;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.zovi.spms.dao.ParkDAO;
import com.zovi.spms.entity.PageBean;
import com.zovi.spms.entity.Park;

@Transactional
public class ParkServiceImpl implements ParkService {
	private ParkDAO parkDAO;
	public ParkDAO getParkDAO() {
		return parkDAO;
	}

	public void setParkDAO(ParkDAO parkDAO) {
		this.parkDAO = parkDAO;
	}

	@Override
	public void add(Park park) {
		parkDAO.add(park);
	}

	@Override
	public void update(Park park) {
		parkDAO.update(park);
	}

	@Override
	public List<Park> showAll() {
		return parkDAO.showAll();
	}
	@Override
	public Park findById(Integer pid) {
		// TODO Auto-generated method stub
		
		return parkDAO.findById(pid);
	}
	@Override
	public PageBean<Park> findByPage(Integer currPage) {
		PageBean<Park> pageBean = new PageBean<Park>();
		//封装当前页数
		pageBean.setCurrPage(currPage);
		//封装每页显示记录数
		int pageSize = 10;
		pageBean.setPageSize(pageSize);
		//封装总记录数
		int totalCount = parkDAO.findCount();
		pageBean.setTotalCount(totalCount);
		//封装总页数
		double tc = totalCount;
		Double num = Math.ceil(tc / pageSize);
		pageBean.setTotalPage(num.intValue());
		//封装每页显示的数据
		int begin = (currPage - 1) * pageSize;
		List<Park> list = parkDAO.findByPage(begin,pageSize);
		pageBean.setList(list);
		return pageBean;
	}

	@Override
	public PageBean<Park> searchByPage(Integer currPage,String entity,String str) {
		PageBean<Park> pageBean = new PageBean<Park>();
		//封装当前页数
		pageBean.setCurrPage(currPage);
		//封装每页显示记录数
		int pageSize = 10;
		pageBean.setPageSize(pageSize);
		//封装总记录数
		int totalCount = parkDAO.findSearchCount(entity, str);
		pageBean.setTotalCount(totalCount);
		//封装总页数
		double tc = totalCount;
		Double num = Math.ceil(tc / pageSize);
		pageBean.setTotalPage(num.intValue());
		//封装每页显示的数据
		int begin = (currPage - 1) * pageSize;
		List<Park> lists = parkDAO.searchByString(begin, pageSize,entity,str);
		pageBean.setList(lists);
		return pageBean;
	}



}
